var gulp        = require('gulp');
var sass        = require('gulp-sass');
var concat = require('gulp-concat');
var cssmin		= require('gulp-cssmin');
var template = require('gulp-template-html');
var rename = require("gulp-rename");
var imagemin = require("gulp-imagemin");

gulp.task('imagemin', function(){
    return gulp.src('src/assets/images/*')
    .pipe(imagemin())
    .pipe(gulp.dest('build/assets/images'))
})
gulp.task('mergehtml', function() {
  return gulp.src('src/include/*.html')
    .pipe(concat('all.html'))
	.pipe(gulp.dest('src/templates'))
	
});
gulp.task('sassdev', () => {
	return gulp .src('src/assets/css/*.scss')
		   		.pipe(sass().on('error', sass.logError))
		   		.pipe(cssmin())
		   		.pipe(gulp.dest('dev/assets/css'))
		  
});


gulp.task('sass', () => {
	return gulp .src('src/assets/css/*.scss')
		   		.pipe(sass().on('error', sass.logError))
		   		// .pipe(cssmin())
		   		.pipe(gulp.dest('build/assets/css'))
	
});
gulp.task('template', function () {
	const glob = require('glob');
	const fileArray = glob.sync('src/*.*');
	for(let i =0,len=fileArray.length; i<len;i++){
		let filename=fileArray[i].replace('src/','');
		
		gulp.src('src/templates/all.html')
		      .pipe(template('src/'+filename))
		      .pipe(rename(function (path) {
			
			    return {
			      dirname: path.dirname + "",
			      basename:filename,
			      extname: ""
			    };
			  }))
		      .pipe(gulp.dest('build'));
	}
});

gulp.task('develop', gulp.series('sass','imagemin','mergehtml','sassdev','template'));
gulp.task('sass:watch', function () {
    gulp.watch('src/assets/sass/*.scss', gulp.series (['sassdev']));
 });

// gulp.task('sass:watch',  gulp.series('develop'));
 